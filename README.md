**ABOUT**
Simple Chat application using Elasticsearch for storing messages.
The backend is a Web Server started via Spark which provides rest services for getting/adding/deleting messages and
websocket for notificaitons from other clients;
Communication with Elasticsearch is done using Apache Jest interface.
Client is a simple single page application using Angular.

## DEMO
http://abrahamcik.cz:8080

## SOURCES
git clone https://abrahamcik@bitbucket.org/abrahamcik/nangutv.git

## LIMITATIONS
There's no authentication. The user is specified by "Login" field in the app.
The application is not very optimized(When user sends message, instead of adding it to the local list, the messages are queried again)
Not very configurable... the limit of 10 messages is hardcoded, no sorting. Filtering only possible on login name and selecting "My messages"

## BUILD
mvn clean compile assembly:single

## RUN
Copy jar with dependencies to server
Copy file chat.properties to the same directory and specify port
java -jar chat-jar-with-dependencies.jar
