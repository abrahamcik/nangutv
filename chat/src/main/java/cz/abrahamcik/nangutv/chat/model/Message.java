package cz.abrahamcik.nangutv.chat.model;

import io.searchbox.annotations.JestId;

import java.util.Date;

public class Message {
    @JestId
    private String id;
    private Date date = new Date();
    private String login;
    private String text;

    public Message(String login, String text) {
        this.login = login;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
