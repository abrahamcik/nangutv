package cz.abrahamcik.nangutv.chat.service;

import com.google.gson.Gson;
import cz.abrahamcik.nangutv.chat.bl.ElasticSearchRepository;
import cz.abrahamcik.nangutv.chat.bl.Repository;
import cz.abrahamcik.nangutv.chat.model.Message;
import cz.abrahamcik.nangutv.chat.model.Response;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.servlet.SparkApplication;

import java.io.FileInputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import static spark.Spark.*;

@WebSocket
public class ChatApplication implements SparkApplication {
    Logger logger = LoggerFactory.getLogger(ChatApplication.class);
    private Repository repository;
    final Set<Session> wsSessions = new HashSet<>();

    public static void main(String[] args) throws Exception {
        final Properties properties = new Properties();
        final FileInputStream in = new FileInputStream("chat.properties");
        properties.load(in);
        final ChatApplication chat = new ChatApplication();
        port(Integer.valueOf(properties.getProperty("port")));
        staticFiles.location("/app");
        webSocket("/chat/ws", chat);
        chat.init();
    }

    @Override
    public void init() {
        this.repository = new ElasticSearchRepository();
        get("/chat/messages", (req, res) -> {
            return new Gson().toJson(repository.getMessages(req.queryParams("login")));
        });
        post("/chat/messages", (req, res) -> {
            repository.storeMessage(new Gson().fromJson(req.body(), Message.class));
            broadcastChange();
            return new Gson().toJson(Response.OK);
        });
        delete("/chat/messages", (req, res) -> {
            repository.deleteMessage(req.queryParams("messageId"));
            broadcastChange();
            return new Gson().toJson(Response.OK);
        });
    }

    private void broadcastChange() {
        wsSessions.stream().filter(Session::isOpen).forEach(session -> {
            try {
                session.getRemote().sendString("UPDATE");
            } catch (Exception e) {
                logger.error("Error notifying clients", e);
            }
        });
    }

    @OnWebSocketConnect
    public void onConnect(Session user) throws Exception {
        wsSessions.add(user);
    }

    @OnWebSocketClose
    public void onClose(Session user, int statusCode, String reason) {
        wsSessions.remove(user);
    }

    @OnWebSocketMessage
    public void onMessage(Session user, String message) {
        //Not needed
    }


}
