package cz.abrahamcik.nangutv.chat.bl;

import com.google.gson.Gson;
import cz.abrahamcik.nangutv.chat.model.Message;
import io.searchbox.core.Delete;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.params.Parameters;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.utils.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class ElasticSearchRepository implements Repository {
    Logger logger = LoggerFactory.getLogger(ElasticSearchRepository.class);

    public List<Message> getMessages(final String login) {
        try {
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            if (StringUtils.isNotEmpty(login)){
                searchSourceBuilder.query(QueryBuilders.matchQuery("login", login));
            }else {
                searchSourceBuilder.query(QueryBuilders.matchAllQuery());
            }
            searchSourceBuilder.size(10);
            searchSourceBuilder.sort(new FieldSortBuilder("date").order(SortOrder.DESC));

            Search search = new Search.Builder(searchSourceBuilder.toString())
                .addIndex("messages")
                .addType("message")
                .build();
            SearchResult result = ElasticSearchDS.getInstance().getClient().execute(search);
            List<SearchResult.Hit<Message, Void>> messages = result.getHits(Message.class);
            return messages.stream().map(e -> e.source).collect(Collectors.toList());
        } catch (final Exception e) {
            logger.error("Could not retreive messages", e);
        }
        return new ArrayList<>();
    }

    @Override
    public void storeMessage(final Message message) {
        try {
            message.setDate(new Date());
            message.setId(UUID.randomUUID().toString());
            final Index index = new Index.Builder(message)
                    .index("messages")
                    .type("message")
                    .setParameter(Parameters.REFRESH, true)
                    .build();
            ElasticSearchDS.getInstance().getClient().execute(index);
        } catch (final Exception e) {
            logger.error("Could not store message", e);
        }
    }

    @Override
    public void deleteMessage(final String id){
        try {
            ElasticSearchDS.getInstance().getClient().execute(new Delete.Builder(id)
                    .index("messages")
                    .type("message")
                    .setParameter(Parameters.REFRESH, true)
                    .build());
        } catch (IOException e) {
            logger.error("Could not delete message", e);
        }
    }
}
