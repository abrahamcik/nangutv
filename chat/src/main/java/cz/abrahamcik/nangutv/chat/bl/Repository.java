package cz.abrahamcik.nangutv.chat.bl;

import cz.abrahamcik.nangutv.chat.model.Message;

import java.util.List;

public interface Repository {

    void storeMessage(Message message);

    List<Message> getMessages(String login);

    void deleteMessage(String id);
}
