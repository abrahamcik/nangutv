package cz.abrahamcik.nangutv.chat.model;

/**
 * Copyright (c) 2018 Frequentis GmbH All Rights Reserved
 * <p/>
 * Response.
 * <p/>
 * $Id$
 * <p/>
 * $HeadURL$
 * <p/>
 *
 * @author user (Last Modified By: $Author$)
 * @version $Revision$
 * @since 4/4/18 3:21 PM (Last Modified on: $Date$)
 */
public class Response {
    final String status;

    public static final Response OK = new Response("success");

    private Response(final String status){
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
