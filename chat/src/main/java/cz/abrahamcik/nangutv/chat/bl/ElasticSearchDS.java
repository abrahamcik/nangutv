package cz.abrahamcik.nangutv.chat.bl;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.indices.CreateIndex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.util.Properties;

public class ElasticSearchDS {
    private static final Logger logger = LoggerFactory.getLogger(ElasticSearchDS.class);

    private static ElasticSearchDS instance = new ElasticSearchDS();

    private JestClient client;

    public static ElasticSearchDS getInstance() {
        return instance;
    }

    private ElasticSearchDS() {
        initClient();
    }

    private void initClient() {
        try {
            final Properties properties = new Properties();
            final FileInputStream in = new FileInputStream("chat.properties");
            properties.load(in);
            JestClientFactory factory = new JestClientFactory();
            factory.setHttpClientConfig(new HttpClientConfig
                .Builder("http://"+properties.getProperty("elasticsearch"))
                .multiThreaded(true)
                .build());
            this.client = factory.getObject();
            this.client.execute(new CreateIndex.Builder("messages").build());
        } catch (Exception e) {
            logger.error("Could not initialize Jest Client", e);
        }
    }

    public JestClient getClient() {
        if (this.client == null) {
            initClient();
        }
        return this.client;
    }
}
