var app = angular.module('app', []);

app.controller('ChatCRUDCtrl', ['$scope', 'ChatCRUDService', function ($scope, ChatCRUDService) {

  $scope.ws = new WebSocket('ws://' + location.host + '/chat/ws');

  $scope.ws.onmessage = function(message){
    $scope.message = 'Message received!';
    $scope.getMessages();
  };

  $scope.addMessage = function () {
    if ($scope.Context != null && $scope.Context.login && $scope.Context.text) {
      ChatCRUDService.addMessage($scope.Context.login, $scope.Context.text)
        .then(function success(response) {
            $scope.message = 'Message added!';
            $scope.errorMessage = '';
            if($scope.Context.myMessagesOnly){
                $scope.getMessages($scope.Context.login);
            } else {
                $scope.getMessages(null);
            }
          },
          function error(response) {
            $scope.errorMessage = 'Error adding Message!';
            $scope.message = '';
          });
    }
    else {
      $scope.errorMessage = 'Please enter a name!';
      $scope.message = '';
    }
  }

  $scope.deleteMessage = function (messageId) {
    ChatCRUDService.deleteMessage(messageId)
      .then(function success(response) {
          $scope.message = 'Message deleted!';
          $scope.Message = null;
          $scope.errorMessage = '';
        },
        function error(response) {
          $scope.errorMessage = 'Error deleting Message!';
          $scope.message = '';
        })
  }

  $scope.getMessages = function () {
    login = null;
    if ($scope.Context.myMessagesOnly){
      login = $scope.Context.login;
    }
    ChatCRUDService.getMessages(login)
      .then(function success(response) {
          $scope.Messages = response.data;
          $scope.errorMessage = '';
        },
        function error(response) {
          $scope.message = '';
          $scope.errorMessage = 'Error getting Messages!';
        });
  }

  $scope.getAllStyle = function (){
      return $scope.getStyle(!$scope.Context.myMessagesOnly);
  }

  $scope.getMyStyle = function (){
      return $scope.getStyle($scope.Context.myMessagesOnly);
  }

  $scope.getStyle = function (highlight){
    if (highlight){
        return { backgroundColor:'gray' };
    }else{
        return {};
    }
  }

  $scope.Context = {};
  $scope.Context.myMessagesOnly = false;
  $scope.getMessages();
}]);

app.service('ChatCRUDService', ['$http', function ($http) {

  this.getMessages = function getMessages(login) {
    return $http({
      method: 'GET',
      url: 'chat/messages',
      params: {login: login}
    });
  }

  this.addMessage = function addMessage(login, text) {
    return $http({
      method: 'POST',
      url: 'chat/messages',
      data: {login: login, text: text}
    });
  }

  this.deleteMessage = function deleteMessage(messageId) {
    return $http({
      method: 'DELETE',
      url: 'chat/messages',
      params: {messageId:messageId}
    })
  }
}]);